# Serverless Rust Microservice

This project demonstrates a serverless microservice built with Rust, deployed as an AWS Lambda function, and connected to an Amazon DynamoDB database. The service is accessible via AWS API Gateway, which provides a RESTful endpoint.

## Overview

The microservice is designed to handle `add` and `retrieve` operations for greetings. When an `add` operation is triggered, the service stores a new greeting message in a DynamoDB table. The `retrieve` operation is intended to fetch a random greeting from the table (note: the implementation of the `retrieve` logic may be pending).

## Architecture

- **AWS Lambda**: The Rust application is packaged and deployed as an AWS Lambda function.
- **Amazon DynamoDB**: The Lambda function interacts with a DynamoDB table, which serves as the database for storing greetings.
- **AWS API Gateway**: An API Gateway is set up to provide HTTP access to the Lambda function, allowing `POST` requests to trigger the `add` operation.

## Setup and Deployment

### Lambda Function

The Rust application is written using the `lambda_runtime` crate and defines a handler function that processes incoming requests.

1. The Lambda function is deployed using the [cargo-lambda](https://github.com/cargo-lambda/cargo-lambda) tool.
2. AWS credentials are set via environment variables:

   ```sh
   export AWS_ACCESS_KEY_ID=your_access_key_id_here
   export AWS_SECRET_ACCESS_KEY=your_secret_access_key_here
   export AWS_REGION=your_aws_region_here
   ```
3. The function is built and deployed with the following commands:
   ```
   cargo build --release
   cargo lambda deploy
   ```
### DynamoDB Table
A DynamoDB table is created through the AWS Management Console with the primary key greeting_id.

### API Gateway
An API Gateway is configured to trigger the Lambda function:

1. A new REST API is created with the default settings.
2. A new resource `/greeting` is added.
3. A method of type `ANY` is set up for the `/greeting` resource, linked to the Lambda function.

### Invocation
The deployed microservice can be invoked via a curl command:

```
curl -X POST https://your-api-id.execute-api.your-region.amazonaws.com/greeting/greeting \
--header "Content-Type: application/json" \
--data '{ "command": "add", "message": "Hello" }'
```

### Results Preview
![Alt text](img/image.png)


### Reference
1. https://stratusgrid.com/blog/aws-lambda-rust-how-to-deploy-aws-lambda-functions
2. https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-create-api-as-simple-proxy-for-lambda.html
3. https://www.cobbing.dev/blog/grappling-a-rust-lambda/
4. https://www.cargo-lambda.info/commands/deploy.html
5. https://coursera.org/groups/building-rust-aws-lambda-microservices-with-cargo-lambda-rd2pc
6. https://github.com/DaGenix/rust-crypto/blob/master/examples/symmetriccipher.rs
7. https://betterprogramming.pub/rust-lambda-and-dynamodb-bea841d47cca
8. https://docs.aws.amazon.com/lambda/latest/dg/with-ddb-example.html