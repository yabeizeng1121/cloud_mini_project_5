use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use rand::Rng;
use serde::{Deserialize, Serialize};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize)]
pub struct Greeting {
    pub greeting_id: String,
    pub message: String,
}

#[derive(Debug, Serialize)]
struct FailureResponse {
    pub body: String,
}

impl std::fmt::Display for FailureResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.body)
    }
}

impl std::error::Error for FailureResponse {}

#[derive(Deserialize)]
struct Request {
    command: String,
    message: Option<String>, // Modified to include an optional message field
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    msg: String,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let r_id = event.context.request_id;
    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);

    match event.payload.command.as_str() {
        "add" => {
            let greeting_id = Uuid::new_v4().to_string();
            // Use the message from the request if provided, otherwise generate a random one
            let message = if let Some(msg) = &event.payload.message {
                msg.clone()
            } else {
                let mut rng = rand::thread_rng();
                let greetings = vec![
                    "Hello, world!", "Hi there!", "Good day!", "Bonjour!", "Hola!",
                    "Guten Tag!", "こんにちは (Konnichiwa)!", "안녕하세요 (Annyeonghaseyo)!",
                    "Привет (Privet)!", "Ciao!", "你好 (Nihao)",
                ];
                greetings[rng.gen_range(0..greetings.len())].to_string()
            };

            let greeting_id_attr = AttributeValue::S(greeting_id.clone());
            let message_attr = AttributeValue::S(message.clone());

            client.put_item()
                .table_name("Greetings")
                .item("greeting_id", greeting_id_attr)
                .item("message", message_attr)
                .send()
                .await
                .map_err(|_err| FailureResponse { body: _err.to_string() })?;

            Ok(Response {
                req_id: r_id,
                msg: format!("Greeting added: {}", message),
            })
        },
        "retrieve" => {
            // Retrieval logic should be implemented here
            Ok(Response {
                req_id: r_id,
                msg: "Retrieving a random greeting requires implementation.".to_string(),
            })
        },
        _ => Err(Error::from(FailureResponse { body: "Invalid command".to_string() })),
    }
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::builder().with_default_directive(LevelFilter::INFO.into()).from_env_lossy())
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}

#[cfg(test)]
mod tests {
    use super::*;
    use lambda_runtime::Context;

    #[tokio::test]
    async fn test_add_greeting_with_specific_message() {
        let request = Request {
            command: "add".to_string(),
            message: Some("Hi there".to_string()),
        };
        let context = Context::default();
        let event = LambdaEvent { payload: request, context };

        let response = function_handler(event).await.expect("Expected Ok response");
        assert!(response.msg.contains("Hi there"));
    }
}
